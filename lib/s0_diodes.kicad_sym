(kicad_symbol_lib
	(version 20231120)
	(generator "kicad_symbol_editor")
	(generator_version "8.0")
	(symbol "SMAJ17A-13-F"
		(pin_names
			(offset 0) hide)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(property "Reference" "D"
			(at 0 2.54 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "SMAJ17A-13-F"
			(at 0 -2.54 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Diode_SMD:D_SMA"
			(at 0 2.54 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" "./s0-kicad-lib/datasheets/Diodes/Diodes_SMAJ17A-13-F.pdf"
			(at 0 2.54 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" "17V unidirectional supressor diode"
			(at 0 0 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Manufacturer" "Diodes"
			(at 0 2.54 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "MPN" "SMAJ17A-13-F"
			(at 0 2.54 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Distributors:" "LCSC: C134963"
			(at 0 2.54 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Marking" "BIUR, UNIR"
			(at 0 0 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "ki_fp_filters" "TO-???* *_Diode_* *SingleDiode* D_*"
			(at 0 0 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(symbol "SMAJ17A-13-F_0_1"
			(polyline
				(pts
					(xy 1.27 1.27) (xy 1.27 -1.27) (xy -1.27 0) (xy 1.27 1.27)
				)
				(stroke
					(width 0.254)
					(type default)
				)
				(fill
					(type none)
				)
			)
			(polyline
				(pts
					(xy 2.54 0) (xy 2.54 0) (xy 2.54 0) (xy -1.27 0)
				)
				(stroke
					(width 0)
					(type default)
				)
				(fill
					(type none)
				)
			)
			(polyline
				(pts
					(xy -1.905 0.635) (xy -1.905 1.27) (xy -1.27 1.27) (xy -1.27 -1.27) (xy -0.635 -1.27) (xy -0.635 -0.635)
				)
				(stroke
					(width 0.254)
					(type default)
				)
				(fill
					(type none)
				)
			)
			(circle
				(center 2.54 0)
				(radius 0.254)
				(stroke
					(width 0)
					(type default)
				)
				(fill
					(type outline)
				)
			)
		)
		(symbol "SMAJ17A-13-F_1_1"
			(pin passive line
				(at -3.81 0 0)
				(length 2.54)
				(name "K"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "1"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin passive line
				(at 5.08 0 180)
				(length 2.54)
				(name "A"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "2"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
		)
	)
	(symbol "SMAJ26A-13-F"
		(pin_names
			(offset 0) hide)
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(property "Reference" "D"
			(at 0 2.54 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Value" "SMAJ26A-13-F"
			(at 0 -2.54 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "Footprint" "Diode_SMD:D_SMA"
			(at 0 2.54 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" "./s0-kicad-lib/datasheets/Diodes/Diodes_SMAJ17A-13-F.pdf"
			(at 0 2.54 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" "26V unidirectional supressor diode"
			(at 0 0 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Manufacturer" "Diodes"
			(at 0 2.54 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "MPN" "SMAJ26A-13-F"
			(at 0 2.54 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Distributors:" "LCSC: C134975"
			(at 0 2.54 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Marking" "BIUR, UNIR"
			(at 0 0 0)
			(effects
				(font
					(size 1.27 1.27)
				)
			)
		)
		(property "ki_fp_filters" "TO-???* *_Diode_* *SingleDiode* D_*"
			(at 0 0 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(symbol "SMAJ26A-13-F_0_1"
			(polyline
				(pts
					(xy 1.27 1.27) (xy 1.27 -1.27) (xy -1.27 0) (xy 1.27 1.27)
				)
				(stroke
					(width 0.254)
					(type default)
				)
				(fill
					(type none)
				)
			)
			(polyline
				(pts
					(xy 2.54 0) (xy 2.54 0) (xy 2.54 0) (xy -1.27 0)
				)
				(stroke
					(width 0)
					(type default)
				)
				(fill
					(type none)
				)
			)
			(polyline
				(pts
					(xy -1.905 0.635) (xy -1.905 1.27) (xy -1.27 1.27) (xy -1.27 -1.27) (xy -0.635 -1.27) (xy -0.635 -0.635)
				)
				(stroke
					(width 0.254)
					(type default)
				)
				(fill
					(type none)
				)
			)
			(circle
				(center 2.54 0)
				(radius 0.254)
				(stroke
					(width 0)
					(type default)
				)
				(fill
					(type outline)
				)
			)
		)
		(symbol "SMAJ26A-13-F_1_1"
			(pin passive line
				(at -3.81 0 0)
				(length 2.54)
				(name "K"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "1"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin passive line
				(at 5.08 0 180)
				(length 2.54)
				(name "A"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "2"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
		)
	)
)
