Stratum 0 KiCad Library
=======================



How to use
----------

* Start your KiCad project as always.
* Add this library as a submodule to your project git.
  The submodule is intended to be in the same directory as the *.kicad_pro* -file.
* Add the schematic and PCB libraries as project/local libs to your project.
* Since this project is a submodule changes in the library will not result in
  changes to your project until you update the submodule.


How to contribute
-----------------

* Just create a MR for this repo.
* There will be a short review for consistency, but we will not check the actual
  design.


Design rules
------------

* Please follow the https://klc.kicad.org/ when creating new symbols / footprints.
  (But we will not be so picky if you don't.)
* Every symbol must have a fooprint associated.
  (Some exceptions may be possible.)

* Every specific (non-standard) part (IC, some connectors) must have
  the following fields:
    * "Manufacturer" 
    * "MPN" (Manufacturer Part Number)

* Every part can contain the following fields:
    * Distributors (Containing Distributor names and their part numbers)

* Every specific part must have a datasheet link.
  Datasheets can either be a link to the manufacturers website.
  Or better: Datasheets can be placed in the *datasheets/* -directory
  and linked using a relative path.

* Footprints shold have a 3D-model.


License
-------

We follow the KiCad Library License.
This is CC-BY-SA 4.0 with the exception that designs based on this library can be
licensed in any arbitrary license (even closed!).
This also allows us to cross-use part of the KiCad Library here.

